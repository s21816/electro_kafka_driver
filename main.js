// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const Kafka = require('node-rdkafka');

var consumer = new Kafka.KafkaConsumer({
  'group.id': 'kafka',
  'metadata.broker.list': 'localhost:9092',
}, {});

var producer = new Kafka.Producer({
  'metadata.broker.list': 'localhost:9092',
  'message.send.max.retries': 5,
});

function sendtotopic(topic_name, argument) {
  try {
    var value = Buffer.from(argument);
    producer.produce(
      topic_name,
      null,
      value,
      'Stormwind',
      Date.now(),
    );
  } catch (err) {
    console.error('A problem occurred when sending our message');
    console.error(err);
  }
}


function createWindow() {      // Create the browser window.
  var mainWindow = new BrowserWindow({
    width: 600,
    height: 400,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })
  mainWindow.loadFile('index.html')     // Load window
  // mainWindow.webContents.openDevTools()

  consumer.connect();         // connect to Kafka
  consumer
    .on('ready', function () {
      consumer.subscribe(['driver']);
      consumer.consume();
    })
    .on('data', function (data) {       // on read perform action
      console.log(data.value.toString())
      mainWindow.webContents.send("data", data.value.toString())
    });
  // setTimeout(function () { mainWindow.webContents.send("data", '{"info":"failure","failure":"coolant","value":126}') }, 4000)

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(
  createWindow
)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
  consumer.disconnect()
  producer.disconnect()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
producer.connect();

// Wait for the ready event before proceeding

function vechicleparam() {
  var params = { "time": "", "oil": "", "coolant": "", "brakes": "" }
  var data = new Date()
  params['time'] = data.getHours() + ':' + data.getMinutes() + ':' + data.getSeconds()
  params['oil'] = Math.floor((50) * Math.random()+ 40)
  params['coolant'] = Math.floor((70) * Math.random() + 50)
  params['brakes'] = ((Math.random() < 0.9) ? 'OK' : 'LOW PRESURE');
  // console.log('params time' + JSON.stringify(params))
  return params


}
// setTimeout(function () { vechicleparam() }, 4000)


producer.on('ready', function () {
  setInterval(function () {
      var argument = JSON.stringify(vechicleparam())
      sendtotopic('chief', argument)
  }, 3000);
  
  ipcMain.on('ask_forstop', (event, arg) => {
    sendtotopic('chief', '{"info": "ask_forstop"}')
  })

})

// Any errors we encounter, including connection errors
producer.on('event.error', function (err) {
  console.error('Error from producer');
  console.error(err);
})

// We must either call .poll() manually after sending messages
// or set the producer to poll on an interval (.setPollInterval).
// Without this, we do not get delivery events and the queue
// will eventually fill up.
// producer.setPollInterval(2);